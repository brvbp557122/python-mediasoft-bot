import telebot
import random

answers = [
  # Положительные
  "Бесспорно",
  "Предрешено",
  "Никаких сомнений",
  "Определённо да",
  "Можешь быть уверен в этом",

  # Нерешительно положительные
  "Мне кажется — «да»",
  "Вероятнее всего",
  "Хорошие перспективы",
  "Знаки говорят — «да»",
  "Да",

  # Нейтральные
  "Пока не ясно, попробуй снова",
  "Спроси позже",
  "Лучше не рассказывать",
  "Сейчас нельзя предсказать",
  "Сконцентрируйся и спроси опять",

  # Отрицательные
  "Даже не думай",
  "Мой ответ — «нет»",
  "По моим данным — «нет»",
  "Перспективы не очень хорошие",
  "Весьма сомнительно"
]


API_TOKEN = '801071433:AAEwqZ1eXYFItX0DPKZDphltPuehjX9biCc'
bot = telebot.TeleBot(API_TOKEN)

@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Привет, ты кто?')

    name = ''

@bot.message_handler(content_types=['text'])
def send_text(message):
    global name
    if message.text and not name:
        name = message.text
        bot.send_message(message.chat.id, 'Привет,{} задай свой вопрос'.format(name))
    elif message.text and name:
        bot.send_message(message.chat.id, answers[random.randint(0, len(answers))])
    elif message.text == 'Пока':
        bot.send_message(message.chat.id, 'Пока, {}, приходи еще!'.format(name))

bot.polling()